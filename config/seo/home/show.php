<?php

return [
    [
        'property' => 'og:url',
        'content' =>  '{{BASE_URL}}',
    ],
    [
        'property' => 'og:title',
        'content' => 'Astian Cloud - Cloud Storage.',
    ],
    [
        'property' => 'og:description',
        'content' => 'Astian Cloud provides secure cloud storage for your photos, videos, music and other files.',
    ],
    [
        'property' => 'keywords',
        'content' => 'files, secure, cloud, storage'
    ],
    [
        'property' => 'og:type',
        'content' => 'website',
    ],
];